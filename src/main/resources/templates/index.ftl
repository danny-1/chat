<html>
<head>
    <title></title>
    <link href="/static/css/semantic.css" rel="stylesheet"/>
    <link href="/static/css/history.css" rel="stylesheet" />
    <style>
body{font-family: "Microsoft YaHei" !important;}
    </style>
</head>
<body>
    <h2 style="margin: 200px auto 20px;width: 100px;" class="ui header">快递搜索</h2>

    <div style="margin: 20px auto;width: 500px;">
    <div class="ui input" >
        <form action="" method="post">
        <input type="text" placeholder="请输入快递单号..." size="50px" name="key" value="${key!}">
        <button class="ui primary button">搜索一下</button>
        </form>
    </div></div>

    <table>

    </table>
    <center class="ui header">${msg!}</center>
    <#if list??>
    <div class="main">
        <div class="history">
            <div class="history-date">
                <ul>
                    <h2 class="first"><a href="#nogo">跟踪记录</a></h2>
                    <li class="green">
                        <h3>10.08<span>2012</span></h3>
                        <dl>
                            <dt>.
                                <span>.</span>
                            </dt>
                        </dl>
                    </li>
                    <#list list as i>
                    <li class="green">
                        <h3>${i.createTime?string('MM.dd')}<span>${i.createTime?string('yyyy')}</span></h3>
                        <dl>
                            <dt>${i.location!}
                                <span>${i.desc!}</span>
                            </dt>
                        </dl>
                    </li>
                    </#list>
                </ul>
            </div>
        </div>
    </div>
    </#if>

    <script type="text/javascript" src="/static/js/jquery.min.js"></script>
    <script type="text/javascript" src="/static/js/semantic.min.js"></script>
    <script type="text/javascript" src="/static/js/main.js"></script>

</body>
</html>