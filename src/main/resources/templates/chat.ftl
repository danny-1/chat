<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="/static/css/layim.css" type="text/css" rel="stylesheet"/>

</head>
<body>

<script src="/static/lay/lib.js"></script>
<script src="/static/lay/layer/layer.min.js"></script>
<script src="/static/lay/layim.js"></script>

<script src="/static/js/sockjs.min.js"></script>


<script>
    // 对Date的扩展，将 Date 转化为指定格式的String
    // 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
    // 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
    // 例子：
    // (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
    // (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18
    Date.prototype.Format = function (fmt) { //author: meizz
        var o = {
            "M+": this.getMonth() + 1, //月份
            "d+": this.getDate(), //日
            "h+": this.getHours(), //小时
            "m+": this.getMinutes(), //分
            "s+": this.getSeconds(), //秒
            "q+": Math.floor((this.getMonth() + 3) / 3), //季度
            "S": this.getMilliseconds() //毫秒
        };
        if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    }

    $(function () {

    })
 var sock = new SockJS('/ws?userId=${me.id}');
 sock.onopen = function() {
     console.log('open');
	 sock.send('open之后需要发送任意字符用于后端注册mapping');
 };
    config.user = {
        name:'${me.name}',
        face:'${me.face}'
    }
 sock.onmessage = function(e) {
     console.log('message', e.data);
     var data = $.parseJSON(e.data);

     if(data.from == ${me.id}){
         log.imarea.append(log.html({
             time: new Date(data.createTime).Format('yyyy-MM-dd hh:mm'),
             name: config.user.name,
             face: config.user.face,
             content: data.content
         }, 'me'));
         node.imwrite.val('').focus();
         log.imarea.scrollTop(log.imarea[0].scrollHeight);
     }
    else if(data.to == ${me.id}){
         log.imarea.append(log.html({
             time: new Date(data.createTime).Format('yyyy-MM-dd hh:mm'),
             name: data.fromUser.name,
             face: data.fromUser.face,
             content: data.content
         }));
         log.imarea.scrollTop(log.imarea[0].scrollHeight);
     }
 };
 sock.onclose = function() {
     console.log('close');
 };
function sendMessage() {
    $.post('')
}
 //sock.close();
</script></body>