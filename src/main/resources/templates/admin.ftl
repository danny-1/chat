<html>
<head>
    <title> - 后台</title>
    <link href="/static/css/semantic.css" rel="stylesheet"/>
    <style>
body{font-family: "Microsoft YaHei" !important;}

<style type="text/css">
body {
    background-color: #FFFFFF;
}
.ui.menu .item img.logo {
    margin-right: 1.5em;
}
.main.container {
    margin-top: 7em;
}
.wireframe {
    margin-top: 2em;
}
.ui.footer.segment {
    margin: 5em 0em 0em;
    padding: 5em 0em;
}
    </style>
</head>
<body>


<div class="ui fixed inverted menu">
    <div class="ui container">
        <a href="#" class="header item">
            ${me.name}，欢迎使用
        </a>
        <a href="/users" class="item">员工管理</a>
        <a href="/sites" class="item">站点管理</a>
        <a href="/region" class="item">地区管理</a>
        <a href="/waybill" class="item">货单管理</a>

    </div>
</div>
<script type="javascript" src="/static/js/semantic.min.js"></script>
</body>
</html>