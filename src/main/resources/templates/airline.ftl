<html>
<head>
    <title> - 后台</title>
    <link href="/static/css/semantic.css" rel="stylesheet"/>
    <style>
body{font-family: "Microsoft YaHei" !important;}
body {
    background-color: #FFFFFF;
}
.ui.menu .item img.logo {
    margin-right: 1.5em;
}
.main.container {
    margin-top: 7em;
}
.wireframe {
    margin-top: 2em;
}
.ui.footer.segment {
    margin: 5em 0em 0em;
    padding: 5em 0em;
}
* { -ms-word-wrap: break-word; word-wrap: break-word; }
html { -webkit-text-size-adjust: none; text-size-adjust: none; }
html, body {height:100%;width:100%; }
html, body, h1, h2, h3, h4, h5, h6, div, ul, ol, li, dl, dt, dd, iframe, textarea, input, button, p, strong, b, i, a, span, del, pre, table, tr, th, td, form, fieldset, .pr, .pc { margin: 0; padding: 0; word-wrap: break-word; font-family: verdana,Microsoft YaHei,Tahoma,sans-serif; *font-family: Microsoft YaHei,verdana,Tahoma,sans-serif; }
body, ul, ol, li, dl, dd, p, h1, h2, h3, h4, h5, h6, form, fieldset, .pr, .pc, em, del { font-style: normal; font-size: 100%; }
ul, ol, dl { list-style: none; }
._citys { width: 450px; display: inline-block; border: 2px solid #eee; padding: 5px; position: relative;background: #fff }
._citys span { color: #56b4f8; height: 15px; width: 15px; line-height: 15px; text-align: center; border-radius: 3px; position: absolute; right: 10px; top: 10px; border: 1px solid #56b4f8; cursor: pointer; }
._citys0 { width: 100%; height: 34px; display: inline-block; border-bottom: 2px solid #56b4f8; padding: 0; margin: 0; }
._citys0 li { display: inline-block; line-height: 34px; font-size: 15px; color: #888; width: 80px; text-align: center; cursor: pointer; }
.citySel { background-color: #56b4f8; color: #fff !important; }
._citys1 { width: 100%; display: inline-block; padding: 10px 0; }
._citys1 a { width: 83px; height: 35px; display: inline-block; background-color: #f5f5f5; color: #666; margin-left: 6px; margin-top: 3px; line-height: 35px; text-align: center; cursor: pointer; font-size: 13px; overflow: hidden; }
._citys1 a:hover { color: #fff; background-color: #56b4f8; }
.AreaS { background-color: #56b4f8 !important; color: #fff !important; }
    </style>
</head>
<body>
    <table class="ui celled striped table">
        <thead>
        <tr><th colspan="4">航班管理
            <button class="button ui primary" onclick="$('.ui.modal').modal('show');">添加</button>
        </th>
        </tr>
        <tr>

            <th>编号</th>
            <th>始发地</th>
            <th>目的地</th>
            <th>始发时间</th>
            <th>到达时间</th>
        </tr>
        </thead>
        <tbody>

        <#list list as i>
        <tr>
            <td>${i.id}</td>
            <td>${i.from}</td>
            <td>${i.to}</td>
            <td>${i.beginTime?string('yyyy-MM-dd HH:mm:ss')}</td>
            <td>${i.endTime?string('yyyy-MM-dd HH:mm:ss')}</td>
        </tr>
        </#list>
        </tbody>
    </table>
<div class="ui modal" style="width: 300px; padding: 15px;margin-left: -200px">
    <form class="ui form five" method="post" form-ajax forward="/airline">
        <div class="field">
            <input type="text" name="from" placeholder="出发地" required/>
        </div>
        <div class="field">
            <input type="text" name="to" placeholder="目的地" required>
        </div>
        <div class="field">
            <input type="" name="beginTime" placeholder="始发时间" required class="laydate-icon" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">
        </div>
        <div class="field">
            <input type="" name="endTime" placeholder="到达时间" required class="laydate-icon" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">
        </div>
        <button class="ui button primary" type="submit">提交</button>
        <button class="ui button" type="button" onclick="$('.ui.modal').modal('hide');">取消</button>
    </form>
</div>
<script type="text/javascript" src="/static/js/jquery.min.js"></script>
<script type="text/javascript" src="/static/js/form-ajax.js"></script>
<script type="text/javascript" src="/static/laydate/laydate.js"></script>
<script type="text/javascript" src="/static/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/static/js/semantic.min.js"></script>
    <script src="/static/js/Popt.js"></script>
    <script src="/static/js/cityJson.js"></script>
    <script src="/static/js/citySet.js"></script>

    <script type="text/javascript">
function add() {
    $.post('/user/add',{
        name:$('#name').val(),
        username: $('#loginname').val(),
        password: $('#password').val(),
    },function (data) {
        alert('添加成功');
        location.reload();
    })
}
function del(id) {
    $.post('/user/delete/'+id,function (data) {
        alert('删除成功');
        location.reload();
    })
}

$("[name=from],[name=to]").click(function (e) {
    SelCity(this,e);
});

</script>
</body>
</html>