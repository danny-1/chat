<html>
<head>
    <title> - 后台</title>
    <link href="/static/css/semantic.css" rel="stylesheet"/>
    <style>
body{font-family: "Microsoft YaHei" !important;}
body {
    background-color: #FFFFFF;
}
.ui.menu .item img.logo {
    margin-right: 1.5em;
}
.main.container {
    margin-top: 7em;
}
.wireframe {
    margin-top: 2em;
}
.ui.footer.segment {
    margin: 5em 0em 0em;
    padding: 5em 0em;
}
    </style>
</head>
<body>
    <table class="ui celled striped table">
        <thead>
        <tr><th colspan="4">聊天记录
            <#--<button class="button ui primary" onclick="$('.ui.modal').modal('show');">添加</button>-->
        </th>
        </tr>
        <tr>
            <th>时间</th>
            <th>昵称</th>
            <th>内容</th>
        </tr>
        </thead>
        <tbody>

        <#list list as i>
        <tr>
            <td class="collapsing"> ${i.createTime?string('yyyy-MM-dd HH:mm:ss')} </td>
            <td>${i.fromName!}</td>
            <td>${i.content!}</td>
        </tr>
        </#list>
        </tbody>
    </table>
<script type="text/javascript" src="/static/js/jquery.min.js"></script>
<script type="text/javascript" src="/static/js/form-ajax.js"></script>
<script type="text/javascript" src="/static/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/static/js/semantic.min.js"></script>
<script type="text/javascript">
</script>
</body>
</html>