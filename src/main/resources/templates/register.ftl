<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link rel="stylesheet" href="/static/css/reset.css" />
    <link rel="stylesheet" href="/static/css/login.css" />
    <script type="text/javascript" src="/static/js/jquery.min.js"></script>
    <script type="text/javascript" src="/static/js/login.js"></script>
</head>
<body>
<div class="page">
    <div class="loginwarrp">
        <div class="logo">欢迎注册</div>
        <div class="login_form">
            <form id="Login" name="Login" method="post" action="/user/add" form-ajax forward="/login">
                <li class="login-item">
                    <span>用户名：</span>
                    <input type="text" id="username" name="username" class="login_input" >
                </li>
                <li class="login-item">
                    <span>密　码：</span>
                    <input type="password" id="password" name="password" class="login_input" >
                </li>
                <li class="login-item">
                    <span>密码确认：</span>
                    <input type="password" id="" name="" class="login_input" >
                </li>
                <li class="login-item">
                    <span>昵称：</span>
                    <input type="text" id="" name="name" class="login_input" >
                </li>
                <li class="login-item">
                    <span>性别</span>
                    <input type="radio" id="" name="sex" class="login_input" value="男"/>男
                    <input type="radio" id="" name="sex" class="login_input" value="女"/>女
                </li>
                <li class="login-item">
                    <span>生日：</span>
                    <input type="text" id="" name="birth" class="login_input" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
                </li>
                <li class="login-sub">
                    <input type="submit" name="Submit" value="提交" />
                                        <input type="button" name="Reset" value="返回登录" onclick="location.href='/login'"/>
                </li>
            </form>
        </div>
    </div>
</div>
<script src="/static/laydate/laydate.js"></script>
<script src="/static/js/form-ajax.js"></script>
<script src="/static/js/jquery.validate.min.js"></script>
<script type="text/javascript">
    window.onload = function() {
        var config = {
            vx : 4,
            vy : 4,
            height : 2,
            width : 2,
            count : 100,
            color : "121, 162, 185",
            stroke : "100, 200, 180",
            dist : 6000,
            e_dist : 20000,
            max_conn : 10
        }
        CanvasParticle(config);
    }
</script>
<script type="text/javascript" src="/static/js/canvas-particle.js"></script>
</body>
</html>