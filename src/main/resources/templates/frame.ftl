<html>
<head>
    <title> - 后台</title>
    <link href="/static/css/semantic.css" rel="stylesheet"/>
    <style>
body{font-family: "Microsoft YaHei" !important;}
body {
    background-color: #FFFFFF;
}
.ui.menu .item img.logo {
    margin-right: 1.5em;
}
.main.container {
    margin-top: 7em;
}
.wireframe {
    margin-top: 2em;
}
.ui.footer.segment {
    margin: 5em 0em 0em;
    padding: 5em 0em;
}
    </style>
</head>
<body>
<div class="ui fixed inverted menu">
    <div class="ui container">
        <a href="#" class="header item">
        欢迎你，${me.name}
        </a>
        <a href="/register" class="item" target="main">注册</a>
        <a href="/chat" class="item" target="main">聊天</a>
        <a href="/users" class="item" target="main">好友</a>
        <a href="/login" class="item" >退出</a>

    </div>
</div>


<div class="" style="margin: 40px;padding: 10px;">
<iframe name="main" src="" style="width:100%;border: none;height: 550px;"></iframe>
</div>
<script type="text/javascript" src="/static/js/jquery.min.js"></script>
<script type="text/javascript" src="/static/js/semantic.min.js"></script>
<script type="text/javascript">
function add() {
    $.post('/user/add',{
        name:$('#name').val(),
        username: $('#loginname').val(),
        password: $('#password').val(),
    },function (data) {
        alert('添加成功');
        location.reload();
    })
}
function del(id) {
    $.post('/user/delete/'+id,function (data) {
        alert('删除成功');
        location.reload();
    })
}
</script>
</body>
</html>