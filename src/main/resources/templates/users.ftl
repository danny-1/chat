<html>
<head>
    <title> - 后台</title>
    <link href="/static/css/semantic.css" rel="stylesheet"/>
    <style>
body{font-family: "Microsoft YaHei" !important;}
body {
    background-color: #FFFFFF;
}
.ui.menu .item img.logo {
    margin-right: 1.5em;
}
.main.container {
    margin-top: 7em;
}
.wireframe {
    margin-top: 2em;
}
.ui.footer.segment {
    margin: 5em 0em 0em;
    padding: 5em 0em;
}
    </style>
</head>
<body>
    <table class="ui celled striped table">
        <thead>
        <tr><th colspan="4">用户列表
            <#--<button class="button ui primary" onclick="$('.ui.modal').modal('show');">添加</button>-->
        </th>
        </tr>
        <tr>
            <th>编号</th>
            <th>用户名</th>
            <th>昵称</th>
            <th>性别</th>
            <th>生日</th>
            <th>添加好友</th>
        </tr>
        </thead>
        <tbody>

        <#list list as i>
        <tr>
            <td class="collapsing"> ${i.id} </td>
            <td>${i.username}</td>
            <td>${i.name}</td>
            <td>${i.sex}</td>
            <td>${i.birth?string('yyyy-MM-dd')}</td>
            <td class="right aligned collapsing" style="text-align: center">
                <#if i.id == me.id>
                    自己
                <#else>
                    <form action="/user/friend/${i.id}" method="get" form-ajax forward="/users">
                        <#if i.isFriend>
                            <button class="button ui red" type="submit">取消好友关系</button>
                        <#else >
                            <button class="button ui green" type="submit">添加好友</button>
                        </#if>
                    </form>
                </#if>
            </td>
        </tr>
        </#list>
        </tbody>
    </table>
<div class="ui modal" style="width: 300px; padding: 15px;margin-left: -200px">
    <form class="ui form five" method="post">
        <div class="field">
            <input type="text" id="name" placeholder="用户名">
        </div>
        <div class="field">
            <input type="text" id="loginname" placeholder="登录名">
        </div>
        <div class="field">
            <input type="password" id="password" placeholder="密码">
        </div>
        <button class="ui button primary" type="button" onclick="add()">提交</button>
        <button class="ui button" type="button" onclick="$('.ui.modal').modal('hide');">取消</button>
    </form>
</div>
<script type="text/javascript" src="/static/js/jquery.min.js"></script>
<script type="text/javascript" src="/static/js/form-ajax.js"></script>
<script type="text/javascript" src="/static/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/static/js/semantic.min.js"></script>
<script type="text/javascript">
function add() {
    $.post('/user/add',{
        name:$('#name').val(),
        username: $('#loginname').val(),
        password: $('#password').val(),
    },function (data) {
        alert('添加成功');
        location.reload();
    })
}
function del(id) {
    $.post('/user/delete/'+id,function (data) {
        alert('删除成功');
        location.reload();
    })
}
</script>
</body>
</html>