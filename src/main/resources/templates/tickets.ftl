<html>
<head>
    <title> - 后台</title>
    <link href="/static/css/semantic.css" rel="stylesheet"/>
    <style>
body{font-family: "Microsoft YaHei" !important;}
body {
    background-color: #FFFFFF;
}
.ui.menu .item img.logo {
    margin-right: 1.5em;
}
.main.container {
    margin-top: 7em;
}
.wireframe {
    margin-top: 2em;
}
.ui.footer.segment {
    margin: 5em 0em 0em;
    padding: 5em 0em;
}
    </style>
</head>
<body>

<form class="ui form" action="" method="get">
    <div class="form-group">
        <label>出发地或者目的地：</label>
        <input type="text" class="form-control input-sm" placeholder="搜索出发地或者目的地" name="key" value="${key! }">
    </div>
    <button type="submit" class="ui button">搜索</button>
</form>
    <table class="ui celled striped table">
        <thead>
        <tr><th colspan="4">票价管理
        <#if me.type='M'><button class="button ui primary" onclick="$('.ui.modal').modal('show');">添加</button></#if>
        </th>
        </tr>
        <tr>
            <th>编号</th>
            <th>网站名称</th>
            <th>价格</th>
            <th>出发地</th>
            <th>目的地</th>
            <th>预计起飞时间</th>
            <th>预计到达时间</th>
        </tr>
        </thead>
        <tbody>

        <#list list as i>
        <tr>
            <td>${i.id}</td>
            <td>${i.companyName}</td>
            <td>${i.price}</td>
            <td>${i.airline.from}</td>
            <td>${i.airline.to}</td>
            <td>${i.airline.beginTime?string('yyyy-MM-dd HH:mm')}</td>
            <td>${i.airline.endTime?string('yyyy-MM-dd HH:mm')}</td>
        </tr>
        </#list>
        </tbody>
    </table>
<div class="ui modal" style="width: 300px; padding: 15px;margin-left: -200px">
    <form class="ui form five" method="post" form-ajax forward="/tickets">
        <div class="field">
            选择航线
            <select name="airlineId">
                <#list airlines as i>
                    <option value="${i.id}">${i.from} - ${i.to} ${i.createTime?string('yyyy-MM-dd HH:mm')}</option>
                </#list>
            </select>
        </div>
        <div class="field">
            选择公司
            <select name="companyId">
            <#list companys as i>
                <option value="${i.id}">${i.name}</option>
            </#list>
            </select>
        </div>
        <div class="field">
            输入价格
            <input type="number" name="price" placeholder="请输入价格">
        </div>
        <button class="ui button primary" type="submit">提交</button>
        <button class="ui button" type="button" onclick="$('.ui.modal').modal('hide');">取消</button>
    </form>
</div>
<script type="text/javascript" src="/static/js/jquery.min.js"></script>
<script type="text/javascript" src="/static/js/semantic.min.js"></script>
<script type="text/javascript" src="/static/js/form-ajax.js"></script>
<script type="text/javascript" src="/static/laydate/laydate.js"></script>
<script type="text/javascript" src="/static/js/jquery.validate.min.js"></script>
<script type="text/javascript">
function add() {
    $.post('/user/add',{
        name:$('#name').val(),
        username: $('#loginname').val(),
        password: $('#password').val(),
    },function (data) {
        alert('添加成功');
        location.reload();
    })
}
function del(id) {
    $.post('/user/delete/'+id,function (data) {
        alert('删除成功');
        location.reload();
    })
}
</script>
</body>
</html>