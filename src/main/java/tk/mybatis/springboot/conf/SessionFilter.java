package tk.mybatis.springboot.conf;

import org.springframework.context.annotation.Configuration;
import tk.mybatis.springboot.model.Users;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by denglei on 2017/4/29.
 */
@WebFilter("/*")
@Configuration
public class SessionFilter implements Filter{
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {


        HttpServletRequest httpServletRequest = (HttpServletRequest)servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse)servletResponse;

        if(
                !httpServletRequest.getRequestURI().contains("login") &&
                !httpServletRequest.getRequestURI().startsWith("/static") &&
                !httpServletRequest.getRequestURI().startsWith("/register") &&
                !httpServletRequest.getRequestURI().startsWith("/user/add") &&
                        null == httpServletRequest.getSession().getAttribute("me")
                ){

            httpServletResponse.sendRedirect("/login");
        }
        else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {

    }
}
