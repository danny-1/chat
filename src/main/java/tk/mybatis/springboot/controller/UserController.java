package tk.mybatis.springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import tk.mybatis.springboot.mapper.FriendshipMapper;
import tk.mybatis.springboot.mapper.UserMapper;
import tk.mybatis.springboot.model.FriendsList;
import tk.mybatis.springboot.model.Friendship;
import tk.mybatis.springboot.model.Message;
import tk.mybatis.springboot.model.Users;
import tk.mybatis.springboot.util.UserUtil;

import java.util.*;

/**
 * Created by denglei on 2017/4/12.
 */
@Controller
@RequestMapping("/user")
@Transactional
public class UserController {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private FriendshipMapper friendshipMapper;

    @RequestMapping("/add")
    @ResponseBody
    public Object add(Users obj){
        obj.setType("E");
        obj.setFace(getFace());
        return userMapper.insertSelective(obj);
    }
    @RequestMapping("/delete/{id}")
    @ResponseBody
    public Object delete(@PathVariable Integer id){
        return userMapper.deleteByPrimaryKey(id);
    }

    /**添加好友，注意是双向的
     * @param friendId
     * @return
     */
    @RequestMapping(value = "/friend/{friendId}",method = RequestMethod.GET)
    @ResponseBody
    public Object addFriend(@PathVariable Integer friendId){
        Users me = UserUtil.getLoginUser();
        Friendship param = new Friendship() {
            {
                setMyId(me.getId());
                setFriendId(friendId);
            }
        };
        Friendship param2 = new Friendship() {
            {
                setFriendId(me.getId());
                setMyId(friendId);
            }
        };
        int i = friendshipMapper.selectCount(param);
        int j = friendshipMapper.selectCount(param2);
        if(i > 0 || j > 0) {
            friendshipMapper.delete(param);
            friendshipMapper.delete(param2);
        }
        else {
            param.setCreateTime(new Date());
            param2.setCreateTime(new Date());
            friendshipMapper.insert(param);
            friendshipMapper.insert(param2);
        }
        return new Message();
    }

    public static String[] faces = new String[]{
            "/static/images/face/1.jpg",
            "/static/images/face/2.jpg",
            "/static/images/face/3.jpg",
            "/static/images/face/4.jpg",
            "/static/images/face/5.jpg",
            "/static/images/face/6.jpg",
    };
    static Random random = new Random();
    public static String getFace(){
        return faces[random.nextInt(faces.length)];
    }

    @RequestMapping(value = "/list_friend")
    @ResponseBody
    public Object listFriend(){
        Users me = UserUtil.getLoginUser();
        List<Users> userss = friendshipMapper.listFriends(me.getId());

        List<FriendsList.Item> items = new ArrayList<>();

        for (Users users : userss) {
            items.add(new FriendsList.Item(users.getId()+"",users.getName(),users.getFace()));
        }
        FriendsList.Group group = new FriendsList.Group("在线好友",userss.size(),1,items);
        return new FriendsList(1,"ok",Arrays.asList(group));
    }
}
