package tk.mybatis.springboot.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tk.mybatis.springboot.model.Users;

/**
 * Created by Administrator on 2016/7/29.
 */
public class UserUtil {
    static Logger logger = LoggerFactory.getLogger(UserUtil.class);


    private UserUtil() {


    }

    public static Users getLoginUser() {
        return  (Users) ServerUtil.getHttpServletRequest().getSession().getAttribute("me");
    }


}
