package tk.mybatis.springboot.model;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.Date;

/**
 * Created by denglei on 2017/4/12.
 */
@Data
public class Users {
    @Id
    private Integer id;
    private String username;
    private String password;
    private String name;
    private String type;
    private byte[] avatar;
    private String sex;
    private String face;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birth;

    @Transient
    private Boolean isFriend;
}
