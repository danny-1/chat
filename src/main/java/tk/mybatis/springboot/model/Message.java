package tk.mybatis.springboot.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by denglei on 2017/5/4.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Message {
    private String message;
    private Boolean success = true;
}
