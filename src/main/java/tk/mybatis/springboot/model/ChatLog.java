package tk.mybatis.springboot.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.Date;

/**
 * Created by denglei on 2017/4/12.
 */
@Data
public class ChatLog {
    @Id
    private Integer id;
    private String content;
    @Column(name = "`from`")
    private Integer from;

    @Transient
    private Users fromUser;
    @Transient
    private String fromName;
    @Column(name = "`to`")
    private Integer to;
    @Transient
    private Users toUser;
    private Date createTime;
}
