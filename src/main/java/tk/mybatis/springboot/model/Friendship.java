package tk.mybatis.springboot.model;

import lombok.Data;

import javax.persistence.Id;
import java.util.Date;

/**
 * Created by denglei on 2017/4/12.
 */
@Data
public class Friendship {
    @Id
    private Integer id;
    private Integer myId;
    private Integer friendId;
    private Date createTime;
}
