package tk.mybatis.springboot.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by denglei on 2017/5/20.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FriendsList {
    private Integer status;
    private String msg;
   private List<Group> data;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Group{
        private String name;
        private Integer nums;
        private Integer id;
        private List<Item> item;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Item{
        private String id;
        private String name;
        private String face;
    }
}
